package com.example.exo3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ContactAdd extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.addButton:

                EditText editText = (EditText)findViewById(R.id.nom);
                String nom = editText.getText().toString();

                editText = (EditText)findViewById(R.id.prenom);
                String prenom = editText.getText().toString();

                editText = (EditText)findViewById(R.id.numTel);
                String numTel = editText.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("contact_nom",nom);
                intent.putExtra("contact_prenom",prenom);
                intent.putExtra("contact_numTel",numTel);

                if(!nom.equals("") && !prenom.equals("")&& !numTel.equals("")){
                    setResult(RESULT_OK,intent);
                    finish();
                }else{
                    Toast.makeText(this,"Veuillez remplir toutes les informations", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.cancelButton:

                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}