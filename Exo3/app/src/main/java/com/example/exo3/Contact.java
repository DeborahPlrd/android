package com.example.exo3;

import androidx.annotation.NonNull;

public class Contact {
    private String nom,prenom,numeroTel;

    Contact(String nom, String prenom, String numeroTel) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroTel = numeroTel;
    }

    @NonNull
    @Override
    public String toString() {
        return nom + " " + prenom + " " + numeroTel;
    }
}

