package com.example.exo3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;
    private ArrayList<Contact> contactList =  new ArrayList<Contact>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String file = readFromFile(this, "contacts.txt");

        String[] contacts = file.split(" && ");

        for (String contact: contacts) {
            String[] infosContact = contact.split(" ");

            Contact contact1 = new Contact(infosContact[0],infosContact[1],infosContact[2]);
            contactList.add(contact1);
        }

        Button button = (Button)findViewById(R.id.addContact);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactAdd.class);

                startActivityForResult(intent,CODE_RES);

            }
        });

        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(
                this,
                android.R.layout.simple_list_item_1,
                contactList
        );

        ListView listView = (ListView)findViewById(R.id.listContact);
        listView.setAdapter(adapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case CODE_RES:
                switch (resultCode){
                    case RESULT_OK:

                        if(data != null){
                            String nom = data.getStringExtra("contact_nom"),
                                    prenom = data.getStringExtra("contact_prenom"),
                                    numTel = data.getStringExtra("contact_numTel");

                            Contact contact = new Contact(
                                    data.getStringExtra("contact_nom"),
                                    data.getStringExtra("contact_prenom"),
                                    data.getStringExtra("contact_numTel")
                            );

                            contactList.add(contact);
                            writeToFile(contact.toString(),this);

                            Toast.makeText(this,"Contact ajouté !", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case RESULT_CANCELED:
                        Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void writeToFile(String data, Context context) {
        try {
            //Writes in contact.txt the data
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);

            //Adds a separator the distinguish our differents datas
            outputStreamWriter.write(" && ");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Toast.makeText(this, "File write failed"+ e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private String readFromFile(Context context, String name) {

        String ret = "";

        try {

            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Toast.makeText(this, "File not found "+ e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, "Can not read file "+ e.toString(), Toast.LENGTH_LONG).show();
        }

        return ret;
    }
}

