package com.example.exo2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Activity2 extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    @Override
    public void onClick(View v) {
        EditText editText = (EditText)findViewById(R.id.nomInput);
        String nom = editText.getText().toString();

        editText = (EditText)findViewById(R.id.prenomInput);
        String prenom = editText.getText().toString();

        editText = (EditText)findViewById(R.id.telInput);
        String tel = editText.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("contact_nom", nom);
        intent.putExtra("contact_prenom", prenom);
        intent.putExtra("contact_tel", tel);

        switch (v.getId()) {
            case R.id.button1:
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.button2:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
