package com.example.exo2;

import androidx.annotation.NonNull;

public class Contact {

    private String nom, prenom, numTel;

    public Contact(String nom, String prenom, String numTel) {
        this.nom = nom;
        this.prenom = prenom;
        this.numTel = numTel;
    }

    @NonNull
    @Override
    public String toString() {
        return nom +" "+ prenom +" "+ numTel;
    }
}
