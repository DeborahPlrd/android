package com.example.exo2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;
    private ArrayList<Contact> contactList = new ArrayList<Contact>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((Button) findViewById(R.id.monBouton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Activity2.class);
                startActivityForResult(intent, CODE_RES);
            }
        });

        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(
                this, android.R.layout.simple_list_item_1,
                contactList);

        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case CODE_RES:
                switch (resultCode) {
                    case RESULT_OK:
                        if(data != null) {
                            Contact contact = new Contact(
                                    data.getStringExtra("contact_nom"),
                                    data.getStringExtra("contact_prenom"),
                                    data.getStringExtra("contact_tel")
                            );

                            contactList.add(contact);
                            Toast.makeText(MainActivity.this, "Contact ajouté", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case RESULT_CANCELED:
                        Toast.makeText(MainActivity.this, "ACTION ANNULEE", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
        }
    }


}
